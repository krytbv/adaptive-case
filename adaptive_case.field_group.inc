<?php

/**
 * @file
 * adaptive_case.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function adaptive_case_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ac_case_settings|node|adaptive_case|form';
  $field_group->group_name = 'group_ac_case_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptive_case';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ac_settings';
  $field_group->data = array(
    'children' => array(
      0 => 'field_ac_hide_game_elements',
      1 => 'field_ac_random_learning_goals',
      2 => 'field_ac_allow_practice_onfinish',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'htab',
    'label' => 'Case',
    'weight' => '10',
  );
  $field_groups['group_ac_case_settings|node|adaptive_case|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ac_difficulty|node|adaptive_case|form';
  $field_group->group_name = 'group_ac_difficulty';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptive_case';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_ac_settings';
  $field_group->data = array(
    'children' => array(
      0 => 'field_ac_lose_streak_threshold',
      1 => 'field_ac_tries_per_question',
      2 => 'field_ac_win_streak_threshold',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'htab',
    'label' => 'Difficulty',
    'weight' => '9',
  );
  $field_groups['group_ac_difficulty|node|adaptive_case|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ac_settings|node|adaptive_case|form';
  $field_group->group_name = 'group_ac_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptive_case';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'group_ac_case_settings',
      1 => 'group_ac_difficulty',
    ),
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
        'id' => '',
      ),
    ),
    'format_type' => 'htabs',
    'label' => 'Settings',
    'weight' => '5',
  );
  $field_groups['group_ac_settings|node|adaptive_case|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ac_tabs|node|adaptive_case_learning_goal|form';
  $field_group->group_name = 'group_ac_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptive_case_learning_goal';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'children' => array(
      0 => 'group_questions',
      1 => 'group_sub_goals',
    ),
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-ac-tabs field-group-tabs',
        'id' => '',
      ),
    ),
    'format_type' => 'tabs',
    'label' => 'Tabs',
    'weight' => '1',
  );
  $field_groups['group_ac_tabs|node|adaptive_case_learning_goal|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_questions|node|adaptive_case_learning_goal|form';
  $field_group->group_name = 'group_questions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptive_case_learning_goal';
  $field_group->mode = 'form';
  $field_group->parent_name = '_add_new_group';
  $field_group->data = array(
    'children' => array(
      0 => 'field_ac_lg_questions',
      1 => 'field_ac_time_per_question',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Questions',
    'weight' => '3',
  );
  $field_groups['group_questions|node|adaptive_case_learning_goal|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sub_goals|node|adaptive_case_learning_goal|form';
  $field_group->group_name = 'group_sub_goals';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'adaptive_case_learning_goal';
  $field_group->mode = 'form';
  $field_group->parent_name = '_add_new_group';
  $field_group->data = array(
    'children' => array(
      0 => 'field_ac_learning_sub_goals',
    ),
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'required_fields' => 1,
      ),
    ),
    'format_type' => 'tab',
    'label' => 'Learning sub-goals',
    'weight' => '4',
  );
  $field_groups['group_sub_goals|node|adaptive_case_learning_goal|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Case');
  t('Difficulty');
  t('Learning sub-goals');
  t('Questions');
  t('Settings');
  t('Tabs');

  return $field_groups;
}
