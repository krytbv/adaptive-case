Drupal.behaviors.adaptiveCase = {
  "attach": function (context, settings) {
    var $ = jQuery;
    var $context = $(context);

    if (!settings.adaptive_case) {
      return;
    }

    var base_url = settings.adaptive_case.base_url;

    if (settings.adaptive_case.question_stats) {
      /**
       * Helper function which converts seconds to a HH:MM:SS format string.
       *
       * @param {integer} seconds
       *
       * @returns {String}
       */
      function toHHMMSS(seconds) {
        var neg_sign = seconds < 0 ? '-' : '';
        seconds = Math.abs(seconds);
        var sec_num = parseInt(seconds, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours > 0) {
          if (hours < 10) {
            hours = "0" + hours;
          }
          hours += 'h';
        } else {
          hours = '';
        }

        if (minutes > 0) {
          if (minutes < 10) {
            minutes = "0" + minutes;
          }
          minutes += 'm';
        } else {
          minutes = '';
        }

        if (seconds > 0) {
          if (seconds < 10) {
            seconds = "0" + seconds;
          }
          seconds += 's';

        } else {
          seconds = '';
        }

        return neg_sign.toString() + hours.toString() + minutes.toString() + seconds.toString();
      }

      /**
       * Init timer
       */
      var show_game_elements = settings.adaptive_case.show_game_elements;
      var time_limit = settings.adaptive_case.level_time_limit;
      var level_start_time = settings.adaptive_case.level_start_time;
      var cq_fastest_time = settings.adaptive_case.question_stats.fastest_time || 0;
      var cq_average_time = settings.adaptive_case.question_stats.average_time || 0;

      var showTimer = (show_game_elements && time_limit > 0) || (show_game_elements && (level_start_time > 0 || cq_fastest_time > 0 || cq_average_time > 0));

      if (showTimer && $('#adaptive_case_timer').length === 0) {
        var $timer = $('<div id="adaptive_case_timer" class="adaptive_case_container" data-debug="added by adaptive_case.js"><table><tr><td class="adaptive_case_running_time adaptive_case_time" title="' + Drupal.t('Time spent on this question.') + '">Time<br /><span></span></td><td class="adaptive_case_max_time adaptive_case_time" title="' + Drupal.t('Time limit for this question.') + '">Max<br /><span></span></td></tr></table></div>');
        if (settings.adaptive_case.show_game_elements) {
          $timer.find('tr').append('<td class="adaptive_case_fastest_time adaptive_case_time" title="' + Drupal.t('Fastest time for this question.') + '">Top<br /><span></span></td>\n\
<td class="adaptive_case_average_time adaptive_case_time" title="' + Drupal.t('Average time for this question.') + '">Avg<br /><span></span></td>\n\
\n\<td class="adaptive_case_badges"></td>');

          // Set time scores.
          $timer.find('.adaptive_case_fastest_time span').html(toHHMMSS(cq_fastest_time) || '-');
          $timer.find('.adaptive_case_average_time span').html(toHHMMSS(cq_average_time) || '-');

          // Add badges
          $timer.find('.adaptive_case_badges').append($('.adaptive_case_badge_container'));
        }

        // Set max time.
        if (time_limit > 0) {
          $timer.find('.adaptive_case_max_time span').html(toHHMMSS(time_limit));
        } else {
          $timer.find('.adaptive_case_max_time').hide();
        }

        $('#closedquestion-get-form-for').prepend($timer);

        // Enable count down timer
        var updateTimer = function () {
          var now = Math.round(new Date().getTime() / 1000);
          var running_time = now - settings.adaptive_case.level_start_time;

          // Update timer
          $timer.find('.adaptive_case_running_time span').html(toHHMMSS(running_time));

          // Update colors of limits
          if (running_time > time_limit - 30) {
            $timer.find('.adaptive_case_max_time').addClass('adaptive_case_timer_almost_expired');
          }
          if (cq_fastest_time > 0 && running_time > cq_fastest_time - 30) {
            $timer.find('.adaptive_case_fastest_time').addClass('adaptive_case_timer_almost_expired');
          }
          if (cq_average_time > 0 && running_time > cq_average_time - 30) {
            $timer.find('.adaptive_case_average_time').addClass('adaptive_case_timer_almost_expired');
          }

          if (running_time > time_limit) {
            $timer.find('.adaptive_case_max_time').addClass('adaptive_case_timer_expired');
          }
          if (cq_fastest_time > 0 && running_time > cq_fastest_time) {
            $timer.find('.adaptive_case_fastest_time').addClass('adaptive_case_timer_expired');
          }
          if (cq_average_time > 0 && running_time > cq_average_time) {
            $timer.find('.adaptive_case_average_time').addClass('adaptive_case_timer_expired');
          }
        }
        window.setInterval(updateTimer, 1000);
        updateTimer();
      }


    }

    /**
     * Move case messages to feedback
     */
    var $cq_score_container = jQuery('.node-closedquestion .adaptive_case_score');
    var $feedback_container = jQuery('.cq_error, .cq_correct');
    if ($cq_score_container.length > 0 && $feedback_container.length > 0) {
      $feedback_container.prepend($cq_score_container);
    }

    /**
     * Prevent user from doing stupid things when clicking reset.
     */
    jQuery('.adaptive_case_reset a').off('click.adaptive_case').on('click.adaptive_case', function (e) {
      if (!confirm(Drupal.t('Are you sure? All your answers will be deleted and your progress will be lost.'))) {
        e.preventDefault();
        return false;
      }
    });
    jQuery('.adaptive_case_reset_all a').off('click.adaptive_case').on('click.adaptive_case', function (e) {
      if (!confirm(Drupal.t('Are you sure? This will reset the case for all users: All their answers will be deleted and their progress will be lost..'))) {
        e.preventDefault();
        return false;
      } else if (!confirm(Drupal.t('Are you really sure?'))) {
        e.preventDefault();
        return false;
      }
    });


    /**
     * Add a 'next question' button to the feedback container
     *
     * Try to see if there is correct feedback. Depending on the order of
     * correct/incorrect feedback, the DOM can have a different structure.
     */

    var correct;
    var $feedbackItem;
    var $feedbackItemContainer;

    if ($context[0].nodeName === '#document' || $context[0].nodeName === 'FORM') {
      $context = $context.find('.cq-feedback-wrapper');
    }

    if ($context.hasClass('cq-feedback-wrapper')) {
      // First try parent.
      correct = $context.closest('.cq_correct').length > 0;

      // Then try child
      if (!correct) {
        correct = $context.find('.cq_correct').length > 0;
      }

      $('.adaptive_case_link').remove();
      if (correct) {
        $feedbackItemContainer = $('.cq_correct').find('.form-item.form-type-item').eq(0).parent();

        $feedbackItem = $('<div class="form-item form-type-item adaptive_case_link"></div>');
        $feedbackItem.html('<a href="' + (window.location.href.split('?')[0]) + '">' + Drupal.t('Click here for next question') + '</a>');

        $feedbackItemContainer.append($feedbackItem);

        $('#edit-submit, #edit-reset').addClass('adaptive_case_button_disabled');
      }
    }
  }
};
