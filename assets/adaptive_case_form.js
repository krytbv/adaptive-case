/**
 * Helper function to determine whether we are in a adaptive case IEF.
 * @param $context
 *   The $context as provided by Drupal.behaviors. *
 * @returns {booleand}
 */
function adaptive_case_is_correct_ief($context) {
  let $contextParent = $context.parent();
  return ($context.parent().hasClass('field-widget-inline-entity-form')
    && ($contextParent.hasClass('field-name-field-ac-learning-sub-goals') || $contextParent.hasClass('field-name-field-ac-learning-goals')));
}

/**
 * Highlight the active IEF.
 */
Drupal.behaviors.adaptiveCaseHighlightActiveEntityForm = {
  "attach": function (context, settings) {
    let $ = jQuery;
    let $context = $(context);

    let disabledClass = 'adaptive_case_form_disabled';
    let hightlightedClass = 'adaptive_case_form_highlighted';

    if (adaptive_case_is_correct_ief($context)) {
      let isFormPresent = $context.find('.ief-form').length >0;

      // First enable everything.
      $('.'+disabledClass).removeClass(disabledClass);

      // Second disable parents.
      let $parents = $context.parents('.field-widget-inline-entity-form');
      if (isFormPresent) {
        $parents.addClass(disabledClass);
      }
      else {
        $parents.removeClass(disabledClass);
      }

      // Third highlight the current IEF.
      if (window.$lastContext) {
        window.$lastContext.removeClass(hightlightedClass);
      }

      $context.addClass(hightlightedClass);
      window.$lastContext = $context;
    }
  }
 }
