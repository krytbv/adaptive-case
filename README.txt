
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Options
 * Installation


INTRODUCTION
------------

The Adaptive Case module provides instructional designers with offering learners and adaptive way of practicing using Closed Questions.
Closed Questions are categorized in Learning Goals. If a learner correctly answers a number of consecutive questions, he finishes the
learning goal. If all Learning Goals have been finished, the learner finishes the Adaptive Case. In case the learner struggles with
questions, he/she is offered to practice with Closed Questions of a Sub-learning Goal. If the learner performs well at the Sub-learning
goal level, he/she will be offered the possibility upwards in below scheme:

┌─────────────────────┐  ┌─────────────────────┐
│   Learning goal 1   │  │   Learning goal m   │
│    - question 1     │  │    - question 1     │
│    - question n     │  │    - question p     │
└──────────┬──────────┘  └─────────────────────┘
           │
           ├─────────────────────────┐
           │                         │
┌──────────▼──────────┐   ┌──────────▼──────────┐
│ Sub-learning goal 1 │   │ Sub-learning goal r │
│    - question 1     ├───►    - question 1     │
│    - question q     │   │    - question s     │
└──────────┬──────────┘   └─────────────────────┘
           │
           ├─────────────────────────┐
           │                         │
┌──────────▼──────────┐   ┌──────────▼──────────┐
│ Sub-learning goal t │   │ Sub-learning goal v │
│    - question 1     ├───►    - question 1     │
│    - question u     │   │    - question w     │
└─────────────────────┘   └─────────────────────┘

OPTIONS
-------

An Adaptive Case entity has the following fields:
* Title
* Introduction: This text will be shown when the learner views the Adaptive Case and he/she did not answer a question yet.
* Learning goals: This is a collection of Learning Goal entities (see below).
* Final message: This text wil be shown when the learner finished the Adaptive Case.
* Difficulty
  -  Winning streak threshold: The number of successful attempts required to move to the next level. A higher number will result in a higher difficulty.
  -  Losing streak threshold: Number of failed attempts before the learner moves back to the previous level. A higher number will result in a lower difficulty.
  -  Number of tries per question: Number of tries the learner has to correctly answer a question. If the learner exceeds the number of tries it will be
     regarded as a failed attempt. A higher number will result in a lower difficulty.
* Case
  -  Randomize learning goals: When checked, learning goals will be randomized.
  -  Hide game elements: When checked, learners will not see their score, stars et cetera.

A Learning Goal entity has the following fields:
* Title
* Questions: A number of Closed Questions belonging to this Learning Goal. The questions will be randomized during the case.
* Time per question: Optional. The number of learner the learner has to answer the question. If the learner requires more time, the attempt will be regarded a
  failure.
* Learning sub-goals: This is a collection of Learning Goal entities

INSTALLATION
------------

Install the module as explained on https://www.drupal.org/docs/7/extend/installing-modules.





Love the limitations, and craziness, of this project.
