<?php

/**
 * @file
 * adaptive_case.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function adaptive_case_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function adaptive_case_node_info() {
  $items = array(
    'adaptive_case' => array(
      'name' => t('Adaptive self-test'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'adaptive_case_learning_goal' => array(
      'name' => t('Adaptive self-test learning goal'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
