<?php

/**
 * @file
 * adaptive_case.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function adaptive_case_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-adaptive_case-body'.
  $field_instances['node-adaptive_case-body'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Introduction',
    'required' => 0,
    'settings' => array(
      'display_summary' => 1,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case-field_ac_allow_practice_onfinish'.
  $field_instances['node-adaptive_case-field_ac_allow_practice_onfinish'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Students will see a list with links to all questions in this case, ordered by learning goal.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_allow_practice_onfinish',
    'label' => 'Let student practice with all questions after finishing the case.',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-adaptive_case-field_ac_finish_page'.
  $field_instances['node-adaptive_case-field_ac_finish_page'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This is what learners will see when they finish the case.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_finish_page',
    'label' => 'Final message',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-adaptive_case-field_ac_hide_game_elements'.
  $field_instances['node-adaptive_case-field_ac_hide_game_elements'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'When checked, learners will not see their score, stars et cetera.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_hide_game_elements',
    'label' => 'Hide game elements',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-adaptive_case-field_ac_learning_goals'.
  $field_instances['node-adaptive_case-field_ac_learning_goals'] = array(
    'bundle' => 'adaptive_case',
    'deleted' => 0,
    'description' => 'These are the learning goals learners should reach to finish this case. Each learning goals consists of a number of questions and sub-learning goals. When learners answer a number of consecutive questions correctly (called a \'winning streak\'), they finish a learning goal. Conversely, if they answer a number of consecutive questions incorrectly (called a \'losing streak\'), they can select sub-learning goals to practice certain aspects of the learning goal.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'entityconnect' => array(
      'button' => array(
        'unload_add_button' => 1,
        'unload_edit_button' => 1,
      ),
      'icon' => array(
        'show_add_icon' => 0,
        'show_edit_icon' => 0,
      ),
    ),
    'field_name' => 'field_ac_learning_goals',
    'label' => 'Learning goals',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 1,
          'allow_existing' => 0,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'learning goals',
          'label_singular' => 'learning goal',
          'match_operator' => 'CONTAINS',
          'override_labels' => 1,
        ),
      ),
      'type' => 'inline_entity_form',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case-field_ac_lose_streak_threshold'.
  $field_instances['node-adaptive_case-field_ac_lose_streak_threshold'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => array(
      0 => array(
        'value' => 2,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of failed attempts before the learner moves back to the previous level. A higher number will result in a lower difficulty.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_lose_streak_threshold',
    'label' => 'Losing streak threshold',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 1,
      'prefix' => '',
      'suffix' => 'failed attempts',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case-field_ac_random_learning_goals'.
  $field_instances['node-adaptive_case-field_ac_random_learning_goals'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'When checked, questions will be randomized. When unchecked, learners will first be served questions belonging to the first learning goal, then questions belonging to the second learning goal, et cetera.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_random_learning_goals',
    'label' => 'Randomize learning goals',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-adaptive_case-field_ac_tries_per_question'.
  $field_instances['node-adaptive_case-field_ac_tries_per_question'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of tries the learner has to correctly answer a question. If the learners exceeds the number of tries it will be regarded as a failed attempt. A higher number will result in a lower difficulty.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_tries_per_question',
    'label' => 'Number of tries per question',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 1,
      'prefix' => '',
      'suffix' => 'tries',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case-field_ac_win_streak_threshold'.
  $field_instances['node-adaptive_case-field_ac_win_streak_threshold'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => array(
      0 => array(
        'value' => 3,
      ),
    ),
    'deleted' => 0,
    'description' => 'The number of successful attempts required to move to the next level. A higher number will result in a higher difficulty.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_win_streak_threshold',
    'label' => 'Winning streak threshold',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 1,
      'prefix' => '',
      'suffix' => 'successful attempts',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'node-adaptive_case-field_xd_courses'.
  $field_instances['node-adaptive_case-field_xd_courses'] = array(
    'bundle' => 'adaptive_case',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_xd_courses',
    'label' => 'Courses',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case_learning_goal-field_ac_learning_sub_goals'.
  $field_instances['node-adaptive_case_learning_goal-field_ac_learning_sub_goals'] = array(
    'bundle' => 'adaptive_case_learning_goal',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'entityconnect' => array(
      'button' => array(
        'unload_add_button' => 1,
        'unload_edit_button' => 1,
      ),
      'icon' => array(
        'show_add_icon' => 0,
        'show_edit_icon' => 0,
      ),
    ),
    'field_name' => 'field_ac_learning_sub_goals',
    'label' => 'Learning sub-goals',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 1,
          'allow_existing' => 1,
          'allow_new' => 1,
          'delete_references' => 0,
          'label_plural' => 'learning sub-goals',
          'label_singular' => 'learning sub-goal',
          'match_operator' => 'CONTAINS',
          'override_labels' => 1,
        ),
      ),
      'type' => 'inline_entity_form',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case_learning_goal-field_ac_lg_questions'.
  $field_instances['node-adaptive_case_learning_goal-field_ac_lg_questions'] = array(
    'bundle' => 'adaptive_case_learning_goal',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'entityconnect' => array(
      'button' => array(
        'unload_add_button' => 1,
        'unload_edit_button' => 1,
      ),
      'icon' => array(
        'show_add_icon' => 0,
        'show_edit_icon' => 0,
      ),
    ),
    'field_name' => 'field_ac_lg_questions',
    'label' => 'Questions',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case_learning_goal-field_ac_time_per_question'.
  $field_instances['node-adaptive_case_learning_goal-field_ac_time_per_question'] = array(
    'bundle' => 'adaptive_case_learning_goal',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'In seconds. Empty or zero means: unlimited.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => '',
        ),
        'type' => 'number_decimal',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_time_per_question',
    'label' => 'Time per question',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case_question_set-field_ac_questions'.
  $field_instances['node-adaptive_case_question_set-field_ac_questions'] = array(
    'bundle' => 'adaptive_case_question_set',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_questions',
    'label' => 'Questions',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-adaptive_case_question_set-field_ac_time_per_question'.
  $field_instances['node-adaptive_case_question_set-field_ac_time_per_question'] = array(
    'bundle' => 'adaptive_case_question_set',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'When provided, learners will see a timer when answering a question. No points will be awarded once the time has passed. Leave empty or 0 to set no time limit.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => '',
        ),
        'type' => 'number_decimal',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ac_time_per_question',
    'label' => 'Time per question',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => 'seconds',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Courses');
  t('Final message');
  t('Hide game elements');
  t('In seconds. Empty or zero means: unlimited.');
  t('Introduction');
  t('Learning goals');
  t('Learning sub-goals');
  t('Let student practice with all questions after finishing the case.');
  t('Losing streak threshold');
  t('Number of failed attempts before the learner moves back to the previous level. A higher number will result in a lower difficulty.');
  t('Number of tries per question');
  t('Number of tries the learner has to correctly answer a question. If the learners exceeds the number of tries it will be regarded as a failed attempt. A higher number will result in a lower difficulty.');
  t('Questions');
  t('Randomize learning goals');
  t('Students will see a list with links to all questions in this case, ordered by learning goal.');
  t('The number of successful attempts required to move to the next level. A higher number will result in a higher difficulty.');
  t('These are the learning goals learners should reach to finish this case. Each learning goals consists of a number of questions and sub-learning goals. When learners answer a number of consecutive questions correctly (called a \'winning streak\'), they finish a learning goal. Conversely, if they answer a number of consecutive questions incorrectly (called a \'losing streak\'), they can select sub-learning goals to practice certain aspects of the learning goal.');
  t('This is what learners will see when they finish the case.');
  t('Time per question');
  t('When checked, learners will not see their score, stars et cetera.');
  t('When checked, questions will be randomized. When unchecked, learners will first be served questions belonging to the first learning goal, then questions belonging to the second learning goal, et cetera.');
  t('When provided, learners will see a timer when answering a question. No points will be awarded once the time has passed. Leave empty or 0 to set no time limit.');
  t('Winning streak threshold');

  return $field_instances;
}
