<?php

/**
 * @file
 * adaptive_case.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function adaptive_case_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_adaptive_case';
  $strongarm->value = '0';
  $export['comment_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_adaptive_case_learning_goal';
  $strongarm->value = '1';
  $export['comment_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_adaptive_case';
  $strongarm->value = 0;
  $export['comment_anonymous_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_adaptive_case_learning_goal';
  $strongarm->value = 0;
  $export['comment_anonymous_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_adaptive_case';
  $strongarm->value = 1;
  $export['comment_default_mode_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_adaptive_case_learning_goal';
  $strongarm->value = 0;
  $export['comment_default_mode_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_adaptive_case';
  $strongarm->value = '50';
  $export['comment_default_per_page_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_adaptive_case_learning_goal';
  $strongarm->value = '50';
  $export['comment_default_per_page_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_adaptive_case';
  $strongarm->value = 0;
  $export['comment_form_location_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_adaptive_case_learning_goal';
  $strongarm->value = 0;
  $export['comment_form_location_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_adaptive_case';
  $strongarm->value = '1';
  $export['comment_preview_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_adaptive_case_learning_goal';
  $strongarm->value = '0';
  $export['comment_preview_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_adaptive_case';
  $strongarm->value = 0;
  $export['comment_subject_field_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_adaptive_case_learning_goal';
  $strongarm->value = 0;
  $export['comment_subject_field_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_adaptive_case';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_adaptive_case_learning_goal';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_adaptive_case';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_adaptive_case_learning_goal';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_adaptive_case';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_adaptive_case_learning_goal';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__adaptive_case';
  $strongarm->value = array(
    'extra_fields' => array(
      'display' => array(),
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
      ),
    ),
    'view_modes' => array(),
  );
  $export['field_bundle_settings_node__adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__adaptive_case_learning_goal';
  $strongarm->value = array(
    'extra_fields' => array(
      'display' => array(),
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
      ),
    ),
    'view_modes' => array(),
  );
  $export['field_bundle_settings_node__adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_adaptive_case';
  $strongarm->value = '0';
  $export['language_content_type_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_adaptive_case_learning_goal';
  $strongarm->value = '0';
  $export['language_content_type_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_adaptive_case';
  $strongarm->value = array(
    0 => 'menu-teacher',
  );
  $export['menu_options_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_adaptive_case_learning_goal';
  $strongarm->value = array();
  $export['menu_options_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_adaptive_case';
  $strongarm->value = 'menu-teacher:715';
  $export['menu_parent_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_adaptive_case_learning_goal';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_adaptive_case';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_adaptive_case_learning_goal';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_adaptive_case';
  $strongarm->value = '1';
  $export['node_preview_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_adaptive_case_learning_goal';
  $strongarm->value = '0';
  $export['node_preview_adaptive_case_learning_goal'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_adaptive_case';
  $strongarm->value = 1;
  $export['node_submitted_adaptive_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_adaptive_case_learning_goal';
  $strongarm->value = 0;
  $export['node_submitted_adaptive_case_learning_goal'] = $strongarm;

  return $export;
}
